package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import fr_binome_07.ArcIn;
import fr_binome_07.InvalidPullException;
import fr_binome_07.Place;

public class ArcInTest {
	private ArcIn a;

	@Before
	public void setUp() throws Exception {
		Place p = new Place(10);
		this.a = new ArcIn(5, p);
	}

	@Test
	public void pullTest() throws InvalidPullException {
		this.a.pull();
		assertEquals(this.a.getPlace().getTokens(), 10-5);
	}

}
