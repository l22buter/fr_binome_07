package tests;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import fr_binome_07.Arc;
import fr_binome_07.ArcEmpty;
import fr_binome_07.ArcIn;
import fr_binome_07.ArcOut;
import fr_binome_07.ArcZero;
import fr_binome_07.InvalidPullException;
import fr_binome_07.PetriNetwork;
import fr_binome_07.Place;
import fr_binome_07.Transition;

public class PetriNetworkTest {
	private PetriNetwork pn;

	@Before
	public void setUp() throws Exception {
		pn = new PetriNetwork();
		Place pIn1 = new Place(7);
		Place pIn2 = new Place(0);
		Place pIn3 = new Place(52);
		Place pOut1 = new Place();
		Place pOut2 = new Place(3);
		ArcIn aIn = new ArcIn(2, pIn1);
		ArcZero aZero = new ArcZero(pIn2);
		ArcEmpty aEmpty = new ArcEmpty(pIn3);
		ArcOut aOut1 = new ArcOut(pOut1);
		ArcOut aOut2 = new ArcOut(27, pOut2);
		Transition t = new Transition(aIn, aZero, aEmpty, aOut1, aOut2);
		pn.addPlaces(pIn1, pIn2, pIn3, pOut1, pOut2);
		pn.addArcs(aIn, aZero, aEmpty, aOut1, aOut2);
		pn.addTransition(t);
	}
	
	@Test
	public void pullTest() throws InvalidPullException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		pn.pull(0);
		
		Transition t = pn.getTransition(0);
		Method privateMethod;
		privateMethod = Transition.class.getDeclaredMethod("getArcs");
		// Set the accessibility as true 
        privateMethod.setAccessible(true); 

        // Store the returned value of 
        // private methods in variable 
        ArrayList<Arc> arcs = (ArrayList<Arc>) privateMethod.invoke(t); 
        
        assertEquals(arcs.get(0).getPlace().getTokens(), 7-2);
        assertEquals(arcs.get(1).getPlace().getTokens(), 0);
        assertEquals(arcs.get(2).getPlace().getTokens(), 0);
        assertEquals(arcs.get(3).getPlace().getTokens(), 0);
        assertEquals(arcs.get(4).getPlace().getTokens(), 3+27);
	}
	
	@Test
	public void addPlaceTest() {
		fail("TO DO");
	}
	
	@Test
	public void addTransitionTest() {
		fail("TO DO");
	}
	
	@Test
	public void addArcTest() {
		fail("TO DO");
	}
	
	@Test
	public void deletePlaceTest() {
		fail("TO DO");
	}
	
	@Test
	public void deleteTransitionTest() {
		fail("TO DO");
	}
	
	@Test
	public void deleteArcTest() {
		fail("TO DO");
	}
	
	@Test
	public void giveTokensTest() {
		fail("TO DO");
	}
	
	@Test
	public void takeTokensTest() {
		fail("TO DO");
	}
	
	@Test
	public void setArcValueTest() {
		fail("TO DO");
	}
	

}
