package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import fr_binome_07.ArcEmpty;
import fr_binome_07.InvalidPullException;
import fr_binome_07.Place;

public class ArcEmptyTest {
	private ArcEmpty a;

	@Before
	public void setUp() throws Exception {
		Place p = new Place(100);
		this.a = new ArcEmpty(p);
	}

	@Test
	public void isPullableTest() throws Exception {
		assertTrue(a.isPullable());
		a.getPlace().takeTokens(a.getPlace().getTokens());
		assertFalse(a.isPullable());
	}
	
	@Test
	public void pullTest() throws InvalidPullException {
		a.pull();
		assertEquals(a.getPlace().getTokens(), 0);
	}

}
