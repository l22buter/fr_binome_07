package tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr_binome_07.Arc;
import fr_binome_07.Place;

public class ArcTest {
	private Arc a;

	@Before
	public void setUp() throws Exception {
		Place p = new Place(7);
		this.a = new Arc(5, p);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void arcTest() throws Exception {
		assertEquals(this.a.getPlace(), new Place(7));
		assertEquals(this.a.getValue(), 5);
	}
	
	@Test
	public void getValueTest() {
		assertEquals(this.a.getValue(), 5);
	}
	
	@Test
	public void setValueTest() {
		this.a.setValue(7);
		assertEquals(this.a.getValue(), 7);
	}
	
	@Test
	public void getPlaceTest() throws Exception {
		assertEquals(this.a.getPlace(), new Place(7));
	}
	
	@Test
	public void isPullableTest() {
		assertTrue(a.isPullable());
		a.setValue(9);
		assertFalse(a.isPullable());
	}
	
	@Test
	public void equalsTest() throws Exception {
		Place p = new Place(7);
		Arc arcSimilar = new Arc(5, p);
		Arc arcEqual = new Arc(5, a.getPlace());
		assertTrue(a.Equals(arcEqual));
		assertFalse(a.Equals(arcSimilar));
	}

}
