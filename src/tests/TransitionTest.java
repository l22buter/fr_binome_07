package tests;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr_binome_07.Arc;
import fr_binome_07.ArcEmpty;
import fr_binome_07.ArcIn;
import fr_binome_07.ArcOut;
import fr_binome_07.ArcZero;
import fr_binome_07.InvalidPullException;
import fr_binome_07.Place;
import fr_binome_07.Transition;

public class TransitionTest {
	private Transition emptyT;
	private Transition t;

	@Before
	public void setUp() throws Exception {
		this.emptyT = new Transition();
		Place pIn1 = new Place(7);
		Place pIn2 = new Place(0);
		Place pIn3 = new Place(52);
		Place pOut1 = new Place();
		Place pOut2 = new Place(3);
		ArcIn aIn = new ArcIn(2, pIn1);
		ArcZero aZero = new ArcZero(pIn2);
		ArcEmpty aEmpty = new ArcEmpty(pIn3);
		ArcOut aOut1 = new ArcOut(pOut1);
		ArcOut aOut2 = new ArcOut(27, pOut2);
		this.t = new Transition(aIn, aZero, aEmpty, aOut1, aOut2);
	}
	
	private Transition notPullableTransition1() throws Exception {
		Place pIn1 = new Place(7);
		Place pIn2 = new Place(0);
		Place pIn3 = new Place(52);
		Place pOut1 = new Place();
		Place pOut2 = new Place(3);
		ArcIn aIn = new ArcIn(24, pIn1);
		ArcZero aZero = new ArcZero(pIn2);
		ArcEmpty aEmpty = new ArcEmpty(pIn3);
		ArcOut aOut1 = new ArcOut(pOut1);
		ArcOut aOut2 = new ArcOut(27, pOut2);
		return new Transition(aIn, aZero, aEmpty, aOut1, aOut2);
	}
	
	private Transition notPullableTransition2() throws Exception {
		Place pIn1 = new Place(7);
		Place pIn2 = new Place(34);
		Place pIn3 = new Place(52);
		Place pOut1 = new Place();
		Place pOut2 = new Place(3);
		ArcIn aIn = new ArcIn(2, pIn1);
		ArcZero aZero = new ArcZero(pIn2);
		ArcEmpty aEmpty = new ArcEmpty(pIn3);
		ArcOut aOut1 = new ArcOut(pOut1);
		ArcOut aOut2 = new ArcOut(27, pOut2);
		return new Transition(aIn, aZero, aEmpty, aOut1, aOut2);
	}
	
	private Transition notPullableTransition3() throws Exception {
		Place pIn1 = new Place(7);
		Place pIn2 = new Place();
		Place pIn3 = new Place();
		Place pOut1 = new Place();
		Place pOut2 = new Place(3);
		ArcIn aIn = new ArcIn(2, pIn1);
		ArcZero aZero = new ArcZero(pIn2);
		ArcEmpty aEmpty = new ArcEmpty(pIn3);
		ArcOut aOut1 = new ArcOut(pOut1);
		ArcOut aOut2 = new ArcOut(27, pOut2);
		return new Transition(aIn, aZero, aEmpty, aOut1, aOut2);
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void isPullableTest() throws Exception {
		assertTrue(this.emptyT.isPullable());
		assertTrue(this.t.isPullable());
		
		assertFalse(this.notPullableTransition1().isPullable());
		assertFalse(this.notPullableTransition2().isPullable());
		assertFalse(this.notPullableTransition3().isPullable());
	}
	
	@Test
	public void pullTest() throws InvalidPullException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		this.t.pull();
		
		Method privateMethod;
		privateMethod = Transition.class.getDeclaredMethod("getArcs");
		// Set the accessibility as true 
        privateMethod.setAccessible(true); 

        // Store the returned value of 
        // private methods in variable 
        ArrayList<Arc> arcs = (ArrayList<Arc>) privateMethod.invoke(this.t); 
        
        assertEquals(arcs.get(0).getPlace().getTokens(), 7-2);
        assertEquals(arcs.get(1).getPlace().getTokens(), 0);
        assertEquals(arcs.get(2).getPlace().getTokens(), 0);
        assertEquals(arcs.get(3).getPlace().getTokens(), 0);
        assertEquals(arcs.get(4).getPlace().getTokens(), 3+27);
	}
	
	@Test(expected = InvalidPullException.class)
	public void pullExceptionTest() throws InvalidPullException, Exception {
		this.notPullableTransition1().pull();
	}
}
