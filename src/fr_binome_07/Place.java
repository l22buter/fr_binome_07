package fr_binome_07;

public class Place {
	private int tokens;

	public Place() {
		this.tokens = 0;
	}
	
	public Place(int tokens) throws Exception {
		if (tokens < 0) {
			throw new Exception("negative tokens");
		}
		this.tokens = tokens;
	}
	
	public int getTokens() {
		return tokens;
	}

	private void setTokens(int tokens) throws Exception {
		if (tokens < 0) {
			throw new Exception("negative tokens");
		}
		this.tokens = tokens;
	}
	
	public void giveTokens(int tokens) throws Exception {
		if (tokens < 0) {
			throw new Exception("negative tokens given");
		}
		this.setTokens(this.getTokens()+tokens);
	}
	
	public void takeTokens(int tokens) throws Exception {
		if (tokens < 0) {
			throw new Exception("negative tokens taken");
		}
		this.setTokens(this.getTokens()-tokens);
	}
	
	public boolean equals(Object o) {
		return o instanceof Place
				&& ((Place)o).getTokens()==this.getTokens();
	}
	
	public String toString() {
		return "Place: " + this.getTokens();
	}
}
