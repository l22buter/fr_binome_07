package fr_binome_07;

import java.util.ArrayList;
import java.util.HashMap;

public class PetriNetwork {
	private HashMap<Integer, Arc> arcs;
	private HashMap<Integer, Place> places;
	private HashMap<Integer, Transition> transitions;
	private int placeId;
	private int arcId;
	private int transitionId;
	
	public PetriNetwork() {
		this.arcs = new HashMap<Integer, Arc>();
		this.places = new HashMap<Integer, Place>();
		this.transitions = new HashMap<Integer, Transition>();
		this.placeId = 0;    //Id of the place to add
		this.arcId = 0;
		this.transitionId = 0;
	}
	
	public HashMap<Integer, Arc> getArcs() {
		return arcs;
	}

	public HashMap<Integer, Place> getPlaces() {
		return places;
	}

	public HashMap<Integer, Transition> getTransitions() {
		return transitions;
	}
	
	public Arc getArc(int id) {
		return this.getArcs().get(id);
	}
	
	public Place getPlace(int id) {
		return this.getPlaces().get(id);
	}
	
	public Transition getTransition(int id) {
		return this.getTransitions().get(id);
	}

	private int getPlaceId() {
		return placeId;
	}

	private void setPlaceId(int placeId) {
		this.placeId = placeId;
	}
	
	private void incrementPlaceId() {
		this.setPlaceId(this.getPlaceId()+1);
	}

	private int getArcId() {
		return arcId;
	}

	private void setArcId(int arcId) {
		this.arcId = arcId;
	}
	
	private void incrementArcId() {
		this.setArcId(this.getArcId()+1);
	}

	private int getTransitionId() {
		return transitionId;
	}

	private void setTransitionId(int transitionId) {
		this.transitionId = transitionId;
	}
	
	private void incrementTransitionId() {
		this.setTransitionId(this.getTransitionId()+1);
	}

	private void pull(Transition t) throws InvalidPullException {
		t.pull();
	}
	
	public void pull(int transitionId) throws InvalidPullException {
		this.pull(this.transitions.get(transitionId));
	}
	
	public int addPlace(Place p) {
		this.incrementPlaceId();
		int id = this.getPlaceId();
		this.getPlaces().put(id, p);
		return id;
	}
	
	public ArrayList<Integer> addPlaces(Place ... places) {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for (Place p : places) {
			ids.add(this.addPlace(p));
		}
		return ids;
	}
	
	public int addTransition(Transition t) {
		this.incrementTransitionId();
		int id = this.getTransitionId();
		this.getTransitions().put(id, t);
		return id;
	}
	
	public ArrayList<Integer> addTransitions(Transition ... transitions) {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for (Transition t : transitions) {
			ids.add(this.addTransition(t));
		}
		return ids;
	}
	
	public int addArc(Arc a) {
		this.incrementArcId();
		int id = this.getArcId();
		this.getArcs().put(id, a);
		return id;
	}
	
	public int addArc(Arc a, int transitionId) {
		this.transitions.get(transitionId).addArc(a);
		return this.addArc(a);
	}
	
	public ArrayList<Integer> addArcs(Arc ... arcs) {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for (Arc a : arcs) {
			ids.add(this.addArc(a));
		}
		return ids;
	}
	
	public void deletePlace(int id) {
		this.getPlaces().remove(id);
	}
	
	public void deleteTransition(int id) {
		this.getTransitions().remove(id);
	}
	
	public void deleteArc(int id) {
		this.getArc(id);
	}
	
	public void giveTokens(int id, int tokens) throws Exception {
		this.getPlace(id).giveTokens(tokens);
	}
	
	public void takeTokens(int id, int tokens) throws Exception {
		this.getPlace(id).takeTokens(tokens);
	}
	
	public void setArcValue(int id, int value) {
		this.getArc(id).setValue(value);
	}
	
}
