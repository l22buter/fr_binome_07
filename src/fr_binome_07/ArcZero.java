package fr_binome_07;

public class ArcZero extends ArcIn{

	public ArcZero(Place p) {
		super(p);
	}
	
	public boolean isPullable() {
		return this.getPlace().getTokens()==0;
	}
	
	public void pull() {
		
	}
	
}
