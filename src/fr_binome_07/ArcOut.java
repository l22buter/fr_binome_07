package fr_binome_07;

public class ArcOut extends Arc {
	
	public ArcOut(Place p) {
		super(p);
	}
	
	public ArcOut(int value, Place p) {
		super(value, p);
	}
	
	public boolean isPullable() {
		return true;
	}
	
	public void pull() throws Exception {
		this.getPlace().giveTokens(getValue());
	}
}
