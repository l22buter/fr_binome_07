package fr_binome_07;

import java.util.ArrayList;
import java.util.Collections;

public class Transition {
	private ArrayList<Arc> arcs;
	
	public Transition() {
		this.arcs = new ArrayList<Arc>();
	}
	
	public Transition(Arc ... arcs) {
		this();
		Collections.addAll(this.getArcs(), arcs);
	}
	
	public Transition(ArrayList<Arc> arcs) {
		this.arcs = arcs;
	}
	
	private ArrayList<Arc> getArcs() {
		return this.arcs;
	}
	
	public void addArc(Arc a) {
		this.arcs.add(a);
	}
	
	public boolean isPullable() {
		for (Arc a : this.getArcs()) {
			if (!a.isPullable()) {
				return false;
			}
		}
		return true;
	}
	
	public void pull() throws InvalidPullException {
		try {
			for (Arc a : this.getArcs()) {
				a.pull();
			}
		} catch (Exception e) {
			throw new InvalidPullException();
		}
	}
}
