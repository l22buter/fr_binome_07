package fr_binome_07;

public class Arc {
	private int value;
	private Place place;
	
	public Arc(Place place) {
		this.setPlace(place);
		this.value = 0;
	}
	public Arc(int value, Place place) {
		this.value = value;
		this.setPlace(place);
	}
	
	public int getValue() {
		return this.value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}
	
	public Place getPlace() {
		return this.place;
	}
	
	private void setPlace(Place place) {
		this.place = place;
	}
	
	public boolean isPullable() {
		return this.getValue() <= this.getPlace().getTokens();
	}
	
	public void pull() throws Exception {
		
	}
	
	public boolean Equals(Object o) {
		// We want the same place in memory
		return o instanceof Arc
				&& ((Arc)o).getPlace()==this.getPlace()
				&& ((Arc)o).getValue()==this.getValue();
	}
	
	public String toString() {
		return "Arc: value=" + this.getValue() + " place=" + this.getPlace().getTokens();
	}
}
